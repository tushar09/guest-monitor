package org.amadeyr.guestmonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.amadeyr.guestmonitor.R;

import java.util.ArrayList;

/**
 * Created by Tushar on 8/18/2016.
 */
public class LVAdapterDetails extends BaseAdapter {

    ArrayList peopleToMeet, dateTime, exitTime, whereToGo;
    Context context;
    LayoutInflater layoutInflater;

    public LVAdapterDetails(Context context, ArrayList peopleToMeet, ArrayList dateTime, ArrayList exitTime, ArrayList whereToGo){
        this.dateTime = dateTime;
        this.peopleToMeet = peopleToMeet;
        this.exitTime = exitTime;
        this.whereToGo = whereToGo;

        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dateTime.size();
    }

    @Override
    public Object getItem(int position) {
        return dateTime.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if(convertView == null){
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.details_row, null);
            holder.dateTime = (TextView) convertView.findViewById(R.id.textView5);
            holder.exitTime = (TextView) convertView.findViewById(R.id.textView4);
            holder.peopleToMeet = (TextView) convertView.findViewById(R.id.textView7);
            holder.whereToGo = (TextView) convertView.findViewById(R.id.textView6);

            convertView.setTag(holder);

        }else{
            holder = (Holder) convertView.getTag();
        }

        holder.whereToGo.setText("" + whereToGo.get(position));
        holder.dateTime.setText("" + dateTime.get(position));
        holder.exitTime.setText("" + exitTime.get(position));
        holder.peopleToMeet.setText("" + peopleToMeet.get(position));

        return convertView;
    }

    class Holder{
        TextView dateTime, exitTime, whereToGo, peopleToMeet;
    }
}
