package org.amadeyr.guestmonitor;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.amadeyr.guestmonitor.database.DBForOff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class GuestProfile extends AppCompatActivity implements View.OnFocusChangeListener{

    private static final int CAMERA_REQUEST = 1888;
    private CircleImageView imageView;

    DBForOff dbo;

    EditText phn, nm, cf, wtg, ptm;
    TextInputLayout phnin, phonenumber, source, destination, ptmin;
    ProgressBar pb;
    Bitmap photo = null;
    Button submit;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbo = new DBForOff(this);

        sp = getSharedPreferences("utils", MODE_PRIVATE);
        editor = sp.edit();


        imageView = (CircleImageView) findViewById(R.id.profile_image);

        phn = (EditText) findViewById(R.id.phn);
        phnin = (TextInputLayout) findViewById(R.id.phnIn);

        nm = (EditText) findViewById(R.id.nm);
        phonenumber = (TextInputLayout) findViewById(R.id.phonenumber);

        wtg = (EditText) findViewById(R.id.wtg);
        destination = (TextInputLayout) findViewById(R.id.destination);

        cf = (EditText) findViewById(R.id.cf);
        source = (TextInputLayout) findViewById(R.id.source);


        ptm = (EditText) findViewById(R.id.ptm);
        ptmin = (TextInputLayout) findViewById(R.id.ptmIn);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        phn.requestFocus();

        submit = (Button) findViewById(R.id.button2);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(photo != null){
                    if(phn.getText().toString() != "" && phn.getText().toString().length() == 11){
                        phnin.setErrorEnabled(false);
                        if(!nm.getText().toString().equals("")){
                            phonenumber.setErrorEnabled(false);
                            if(!cf.getText().toString().equals("")){
                                source.setErrorEnabled(false);
                                if(!wtg.getText().toString().equals("")){
                                    destination.setErrorEnabled(false);
                                    if(!ptm.getText().toString().equals("")){
                                        ptmin.setErrorEnabled(false);
                                        if(photo != null){
                                            String encodeImg = encodeToBase64(photo, Bitmap.CompressFormat.JPEG, 100);
                                            ContextWrapper cw = new ContextWrapper(getApplicationContext());
                                            // path to /data/data/yourapp/app_data/imageDir
                                            File directory = cw.getDir("imageDir", Context.MODE_WORLD_READABLE);
                                            // Create imageDir
                                            File mypath = new File(directory, phn.getText().toString() + ".jpg");

                                            FileOutputStream fos = null;
                                            try {
                                                fos = new FileOutputStream(mypath);
                                                // Use the compress method on the BitMap object to write image to the OutputStream
                                                photo.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                                photoVisitor = mypath.getPath();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            } finally {
                                                //fos.close();
                                            }
                                            //Log.e("encode", encodeImg);
                                            requestEntry(encodeImg, photo);

                                            Toast.makeText(GuestProfile.this, "Successfully Submitted", Toast.LENGTH_SHORT).show();
                                            finish();
                                        }else {
                                            Toast.makeText(GuestProfile.this, "Please Take A Photo Of Guest", Toast.LENGTH_SHORT).show();
                                        }

                                    }else{
                                        Toast.makeText(GuestProfile.this, "Please Provide People to Meet", Toast.LENGTH_SHORT).show();
                                        ptmin.setErrorEnabled(true);
                                        ptmin.setError("Please Provide People to Meet");
                                    }
                                }else{
                                    Toast.makeText(GuestProfile.this, "Phone Provide Where To Go", Toast.LENGTH_SHORT).show();
                                    destination.setErrorEnabled(true);
                                    destination.setError("Please Provide Where To Go");
                                }
                            }else{
                                Toast.makeText(GuestProfile.this, "Please Provide The Source", Toast.LENGTH_SHORT).show();
                                source.setErrorEnabled(true);
                                source.setError("Please Provide The Source");
                            }
                        }else{
                            Toast.makeText(GuestProfile.this, "Please Enter The Name", Toast.LENGTH_SHORT).show();
                            phonenumber.setErrorEnabled(true);
                            phonenumber.setError("Please Enter The Name");
                        }
                    }else{
                        Toast.makeText(GuestProfile.this, "Phone Number is Invalid", Toast.LENGTH_SHORT).show();
                        phnin.setErrorEnabled(true);
                        phnin.setError("Phone Number is Invalid");
                    }
                //}
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        FloatingActionButton fabid = (FloatingActionButton) findViewById(R.id.view);
        fabid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phn.getText().toString() != "" && phn.getText().toString().length() == 11){
                    phnin.setErrorEnabled(false);
                    if(!nm.getText().toString().equals("")){
                        phonenumber.setErrorEnabled(false);
                        if(!cf.getText().toString().equals("")){
                            source.setErrorEnabled(false);
                            if(!wtg.getText().toString().equals("")){
                                destination.setErrorEnabled(false);
                                if(!ptm.getText().toString().equals("")){
                                    ptmin.setErrorEnabled(false);
                                    //if(photo != null){
                                        startActivity(new Intent(GuestProfile.this, IDCard.class).putExtra("phone", phn.getText().toString()));
                                    //}else {
                                        //Toast.makeText(GuestProfile.this, "Please Take A Photo Of Guest", Toast.LENGTH_SHORT).show();
                                    //}

                                }else{
                                    Toast.makeText(GuestProfile.this, "Please Provide People to Meet", Toast.LENGTH_SHORT).show();
                                    ptmin.setErrorEnabled(true);
                                    ptmin.setError("Please Provide People to Meet");
                                }
                            }else{
                                Toast.makeText(GuestProfile.this, "Phone Provide Where To Go", Toast.LENGTH_SHORT).show();
                                destination.setErrorEnabled(true);
                                destination.setError("Please Provide Where To Go");
                            }
                        }else{
                            Toast.makeText(GuestProfile.this, "Please Provide The Source", Toast.LENGTH_SHORT).show();
                            source.setErrorEnabled(true);
                            source.setError("Please Provide The Source");
                        }
                    }else{
                        Toast.makeText(GuestProfile.this, "Please Enter The Name", Toast.LENGTH_SHORT).show();
                        phonenumber.setErrorEnabled(true);
                        phonenumber.setError("Please Enter The Name");
                    }
                }else{
                    Toast.makeText(GuestProfile.this, "Phone Number is Invalid", Toast.LENGTH_SHORT).show();
                    phnin.setErrorEnabled(true);
                    phnin.setError("Phone Number is Invalid");
                }

            }
        });

        phn.setOnFocusChangeListener(this);
        nm.setOnFocusChangeListener(this);
        cf.setOnFocusChangeListener(this);
        wtg.setOnFocusChangeListener(this);
        ptm.setOnFocusChangeListener(this);


    }

    private boolean requestEntryOffLine(String photoPerson) {
        return dbo.insertRow(nm.getText().toString(), phn.getText().toString(), cf.getText().toString(), wtg.getText().toString(), ptm.getText().toString(), getDate(), "inside", sp.getString("userName", ""), photoPerson, "0");
//        if(ins){
//            Toast.makeText(GuestProfile.this, "Inserter", Toast.LENGTH_SHORT).show();
//            try {
//                dbo.syncRequest();
//            } catch (JSONException e) {
//                Toast.makeText(GuestProfile.this, "Data is not saved online. Please check internet", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    String photoVisitor = "";

    private void requestEntry(final String encodeImg, Bitmap img) {
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/submit.php",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("response", response);
////                        JSONObject jsonObject1 = null;
////                        try {
////                            jsonObject1 = new JSONObject(response);
////                            JSONArray jArr = jsonObject1.getJSONArray("FullData");
////                            //JSONObject data = jArr.getJSONObject(0);
////
////                            for (int t = 0; t < 1; t++) {
////                                JSONObject jsonObject = jArr.getJSONObject(t);
////                                nm.setText("" + jsonObject.get("name"));
////                                cf.setText("" + jsonObject.get("cameFrom"));
////                                wtg.setText("" + jsonObject.get("wantToGo"));
////                                ptm.setText("" + jsonObject.get("peopleToMeet"));
////                            }
////                            pb.setVisibility(View.GONE);
////
////                        } catch (JSONException e) {
////                            Log.e("error", e.toString());
////                            e.printStackTrace();
////                            pb.setVisibility(View.GONE);
////                        }
////
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//
//                ContextWrapper cw = new ContextWrapper(getApplicationContext());
//                // path to /data/data/yourapp/app_data/imageDir
//                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//                File file[] = directory.listFiles();
//                Log.e("Files", "Size: "+ file.length);
//                String photoFront = "";
//                String photoBack = "";
//
//                boolean f = false;
//                boolean b = false;
//
//                for (int i=0; i < file.length; i++) {
//                    if(file[i].getName().startsWith(phn.getText().toString()) && file[i].getName().endsWith("front.jpg")){
//                        photoFront = file[i].getPath();
//                        f = true;
//                    }
//                    if(file[i].getName().startsWith(phn.getText().toString()) && file[i].getName().endsWith("back.jpg")){
//                        photoBack = file[i].getPath();
//                        b = true;
//                    }
//                    if(file[i].getName().equals(phn.getText().toString() + ".jpg")){
//                        photoVisitor = file[i].getPath();
//                        //b = true;
//                    }
//                    Log.e("Files", "FilePath:" + file[i].getPath());
//                }
//
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//                Bitmap bitmapFront = BitmapFactory.decodeFile(photoFront, options);
//                Bitmap bitmapBack = BitmapFactory.decodeFile(photoBack, options);
//
//                if(bitmapBack == null){
//                    Log.e("bitmap", "back is null b = " + b);
//                }
//                if(bitmapFront == null){
//                    Log.e("bitmap", "front is null");
//                }
//
//                String enPhotoFront = "";
//                String enPhotoBack = "";
//                if(b && f){
//                    enPhotoFront = encodeToBase64(bitmapFront, Bitmap.CompressFormat.JPEG, 100);
//                    enPhotoBack = encodeToBase64(bitmapBack, Bitmap.CompressFormat.JPEG, 100);
//                }else{
//
//                }
//
//
//
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("userID", sp.getString("userName", ""));
//                params.put("phoneNumber", phn.getText().toString());
//                params.put("name", nm.getText().toString());
//                params.put("cameFrom", cf.getText().toString());
//                params.put("whereToGo", wtg.getText().toString());
//                params.put("peopleToMeet", ptm.getText().toString());
//                params.put("dateTime", getDate());
//                params.put("exitTime", "inside");
//                params.put("image", encodeImg);
//                params.put("syncFlag", "0");
//
//                if(b && f){
//                    params.put("front", enPhotoFront);
//                    params.put("back", enPhotoBack);
//                }else{
//                    params.put("front", "");
//                    params.put("back", "");
//                }
//
//                return params;
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
        requestEntryOffLine(photoVisitor);
        try {
            dbo.syncRequest();
        } catch (JSONException e) {
            Log.e("dbo.syncRequest", e.toString());
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }
    }

    private void autoFill() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/autofill.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject1 = null;
                        if(response.equals("no")){
                            nm.setEnabled(true);
                            cf.setEnabled(true);
                            nm.setText("");
                            cf.setText("");
                            wtg.setText("");
                            ptm.setText("");
                        }
                        try {
                            jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("FullData");
                            //JSONObject data = jArr.getJSONObject(0);

                            for (int t = 0; t < 1; t++) {
                                JSONObject jsonObject = jArr.getJSONObject(t);
                                nm.setText("" + jsonObject.get("name"));
                                cf.setText("" + jsonObject.get("cameFrom"));
                                wtg.setText("" + jsonObject.get("wantToGo"));
                                ptm.setText("" + jsonObject.get("peopleToMeet"));
                                nm.setEnabled(false);
                                cf.setEnabled(false);
                            }
                            pb.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phn.getText().toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);

        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v.getId() == R.id.phn){
            if (!hasFocus){
                if(phn.getText().toString() != "" && phn.getText().toString().length() == 11){
                    pb.setVisibility(View.VISIBLE);
                    autoFill();
                    phnin.setErrorEnabled(false);
                }else{
                    phnin.setErrorEnabled(true);
                    Toast.makeText(GuestProfile.this, "Phone Number is Invalid", Toast.LENGTH_SHORT).show();
                    phnin.setError("Phone Number is Invalid");
                }
            }else {
                pb.setVisibility(View.GONE);
            }
        }

        if(v.getId() == R.id.nm){
            if (!hasFocus){
                if(!nm.getText().toString().equals("")){
                    phnin.setErrorEnabled(false);
                }else{
                    phonenumber.setErrorEnabled(true);
                    Toast.makeText(GuestProfile.this, "Please Enter The Name", Toast.LENGTH_SHORT).show();
                    phonenumber.setError("Please Enter The Name");
                }
            }else {
                //pb.setVisibility(View.GONE);
            }
        }

        if(v.getId() == R.id.cf){
            if (!hasFocus){
                if(!cf.getText().toString().equals("")){
                    source.setErrorEnabled(false);
                }else{
                    source.setErrorEnabled(true);
                    Toast.makeText(GuestProfile.this, "Please Provide The Source", Toast.LENGTH_SHORT).show();
                    source.setError("Please Provide The Source");
                }
            }else {
                //pb.setVisibility(View.GONE);
            }
        }

        if(v.getId() == R.id.wtg){
            if (!hasFocus){
                if(!wtg.getText().toString().equals("")){
                    destination.setErrorEnabled(false);
                }else{
                    destination.setErrorEnabled(true);
                    Toast.makeText(GuestProfile.this, "Please Provide The Destination", Toast.LENGTH_SHORT).show();
                    destination.setError("Please Provide The Destination");
                }
            }else {
                //pb.setVisibility(View.GONE);
            }
        }

        if(v.getId() == R.id.ptm){
            if (!hasFocus){
                if(!ptm.getText().toString().equals("")){
                    ptmin.setErrorEnabled(false);
                }else{
                    ptmin.setErrorEnabled(true);
                    Toast.makeText(GuestProfile.this, "Please Provide The People To Meet", Toast.LENGTH_SHORT).show();
                    ptmin.setError("Please Provide The People To Meet");
                }
            }else {
                //pb.setVisibility(View.GONE);
            }
        }
    }

    public String getDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm a");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
