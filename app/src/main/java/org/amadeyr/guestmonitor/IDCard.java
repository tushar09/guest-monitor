package org.amadeyr.guestmonitor;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class IDCard extends AppCompatActivity {

    ImageButton front, back;
    Bitmap photoFront = null;
    Bitmap photoBack = null;

    String phoneNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idcard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        phoneNumber = getIntent().getStringExtra("phone");

        front = (ImageButton) findViewById(R.id.front);
        back = (ImageButton) findViewById(R.id.back);

        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 456);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 654);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photoBack != null && photoFront != null) {
                    try {
                        saveToInternalStorage(photoFront, photoBack);
                        Toast.makeText(IDCard.this, "Photo Saved", Toast.LENGTH_SHORT).show();
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(IDCard.this, "Something Went Wrong. Try Again", Toast.LENGTH_LONG).show();
                        Log.e("id save error", e.toString());
                    }
                } else {
                    Toast.makeText(IDCard.this, "Please Take Photos", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 456 && resultCode == RESULT_OK) {
            photoFront = (Bitmap) data.getExtras().get("data");
            photoFront = photoFront.createScaledBitmap(photoFront, front.getWidth(), front.getHeight(), true);
            front.setImageBitmap(photoFront);
        }

        if (requestCode == 654 && resultCode == RESULT_OK) {
            photoBack = (Bitmap) data.getExtras().get("data");
            photoBack = photoBack.createScaledBitmap(photoBack, back.getWidth(), back.getHeight(), true);
            back.setImageBitmap(photoBack);
        }
    }

    private void saveToInternalStorage(Bitmap photoFront, Bitmap photoBack) throws IOException {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, phoneNumber + "_nid_front.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            photoFront.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //fos.close();
        }

        ContextWrapper cw2 = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        //File directory2 = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath2 = new File(directory, phoneNumber + "_nid_back.jpg");

        FileOutputStream fos2 = null;
        try {
            fos = new FileOutputStream(mypath2);
            // Use the compress method on the BitMap object to write image to the OutputStream
            photoBack.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fos.close();
        }


        //return directory.getAbsolutePath();
    }

}
