package org.amadeyr.guestmonitor;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.amadeyr.guestmonitor.adapter.LVAdapter;
import org.amadeyr.guestmonitor.adapter.LVAdapterDetails;
import org.amadeyr.guestmonitor.database.DBForOff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VisitorDetails extends AppCompatActivity {

    ArrayList name, phone, dateTime, exitTime, cameFrom, wentTo, peopleToMeet, photo;
    TextView nametv, phonetv, cameFromtv, dateTimetv, exitTimetv, peopleToMeettv;
    ImageView iv;
    ListView lv;
    DBForOff dbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbo = new DBForOff(this);

        nametv = (TextView) findViewById(R.id.textView8);
        cameFromtv = (TextView) findViewById(R.id.textView10);
        phonetv = (TextView) findViewById(R.id.textView9);

        lv = (ListView) findViewById(R.id.listView);
        iv = (ImageView) findViewById(R.id.profile_image);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        autoFill();
    }

    private void autoFill() {
        loadOffLine();
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/visitorDetails.php",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        JSONObject jsonObject1 = null;
//                        Log.e("error", response);
//                        name = new ArrayList();
//                        phone = new ArrayList();
//                        dateTime = new ArrayList();
//                        exitTime = new ArrayList();
//                        cameFrom = new ArrayList();
//                        wentTo = new ArrayList();
//                        peopleToMeet = new ArrayList();
//
//                        try {
//                            jsonObject1 = new JSONObject(response);
//                            JSONArray jArr = jsonObject1.getJSONArray("FullData");
//                            //JSONObject data = jArr.getJSONObject(0);
//
//                            for (int t = 0; t < jArr.length(); t++) {
//                                JSONObject jsonObject = jArr.getJSONObject(t);
//                                name.add("" + jsonObject.get("name"));
//                                phone.add("" + jsonObject.get("phone"));
//                                dateTime.add("" + jsonObject.get("dateTime"));
//                                exitTime.add("" + jsonObject.get("exitTime"));
//                                cameFrom.add("" + jsonObject.get("cameFrom"));
//                                wentTo.add("" + jsonObject.get("wantToGo"));
//                                peopleToMeet.add("" + jsonObject.get("peopleToMeet"));
//
//                            }
//
//                            nametv.setText("" + name.get(0));
//                            phonetv.setText(getIntent().getStringExtra("phone"));
//                            cameFromtv.setText("" + cameFrom.get(0));
//                            Picasso.with(VisitorDetails.this).load(getIntent().getStringExtra("photo")).into(iv);
//                            lv.setAdapter(new LVAdapterDetails(VisitorDetails.this, peopleToMeet, dateTime, exitTime, wentTo));
//
//                        } catch (JSONException e) {
//                            Log.e("error", e.toString());
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.e("error", error.toString());
//                        loadOffLine();
//                    }
//                }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("phone", getIntent().getStringExtra("phone"));
//                return params;
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
    }

    private void loadOffLine() {
        name = new ArrayList();
        phone = new ArrayList();
        dateTime = new ArrayList();
        exitTime = new ArrayList();
        cameFrom = new ArrayList();
        wentTo = new ArrayList();
        peopleToMeet = new ArrayList();
        photo = new ArrayList();
        Cursor cursor = dbo.getVisitorDetails("'" + getIntent().getStringExtra("phone") + "'");
        if (cursor.moveToFirst()){
            do{
                name.add(cursor.getString(cursor.getColumnIndex("name")));
                Log.e("name", cursor.getString(cursor.getColumnIndex("name")));
                dateTime.add(cursor.getString(cursor.getColumnIndex("dateTime")));
                photo.add(cursor.getString(cursor.getColumnIndex("photo")));
                Log.e("dateTime", cursor.getString(cursor.getColumnIndex("dateTime")));
                phone.add(cursor.getString(cursor.getColumnIndex("phone")));
                Log.e("photo", cursor.getString(cursor.getColumnIndex("photo")));
                exitTime.add(cursor.getString(cursor.getColumnIndex("exitTime")));
                Log.e("exitTime", cursor.getString(cursor.getColumnIndex("exitTime")));
                wentTo.add(cursor.getString(cursor.getColumnIndex("wantToGo")));
                peopleToMeet.add(cursor.getString(cursor.getColumnIndex("peopleToMeeet")));
                cameFrom.add(cursor.getString(cursor.getColumnIndex("cameFrom")));
                Log.e("wantToGo", cursor.getString(cursor.getColumnIndex("wantToGo")));
                // do what ever you want here
            }while(cursor.moveToNext());
        }
        cursor.close();
        nametv.setText("" + name.get(0));
        phonetv.setText(getIntent().getStringExtra("phone"));
        cameFromtv.setText("" + cameFrom.get(0));
        Picasso.with(VisitorDetails.this).load(new File("" + photo.get(0))).into(iv);
        lv.setAdapter(new LVAdapterDetails(VisitorDetails.this, peopleToMeet, dateTime, exitTime, wentTo));
    }

}
