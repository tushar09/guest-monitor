package org.amadeyr.guestmonitor;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.amadeyr.guestmonitor.database.DBForOff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tushar on 8/16/2016.
 */
public class Sync {

    Context context;
    DBForOff dbo;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Sync(Context context) {
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        this.context = context;
        dbo = new DBForOff(context);
    }

    public void fillOnDB() {

    }

    public void fillOffDB() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/SyncActionSend.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("not Inserted", response);
                        JSONObject jsonObject1 = null;
                        try {
                            jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("FullData");
                            JSONObject mode = jArr.getJSONObject(0);

                            for (int t = 0; t < jArr.length(); t++) {
                                JSONObject jsonObject = jArr.getJSONObject(t);
                                String name, dateTime, phone, photo, exitTime, id, whereToGo, cameFrom, userId, peopleToMeet;
                                name = "" + jsonObject.get("name");
                                dateTime = "" + jsonObject.get("dateTime");
                                phone = "" + jsonObject.get("phone");
                                //photo.add(jsonObject.get("photo"));
                                photo = "" + jsonObject.get("photo");
                                exitTime = "" + jsonObject.get("exitTime");
                                whereToGo = "" + jsonObject.get("whereToGo");
                                cameFrom = "" + jsonObject.get("cameFrom");
                                userId = "" + jsonObject.get("userId");
                                peopleToMeet = "" + jsonObject.get("peopleToMeet");
                                saveBitmap(photo, phone);
                                ContextWrapper cw = new ContextWrapper(context);
                                // path to /data/data/yourapp/app_data/imageDir
                                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                                // Create imageDir
                                File mypath = new File(directory, phone + ".jpg");
                                if(!mypath.getParentFile().isDirectory()){
                                    mypath.getParentFile().mkdir();
                                }
                                dbo.insertRow(name, phone, cameFrom, whereToGo, peopleToMeet, dateTime, exitTime, userId, mypath.getPath(), "1");
                            }
                            //dbo.deleteDuplicate();

                        } catch (JSONException e) {
                            Log.e("not Inserted1", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("not Inserted2", error.toString());
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", sp.getString("userName", ""));

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }



    private void saveBitmap(final String photo, final String phn) {
        Log.e("photo url", photo);
        Picasso.with(context).load(photo).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                ContextWrapper cw = new ContextWrapper(context);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, phn + ".jpg");
                if(!mypath.getParentFile().isDirectory()){
                    mypath.getParentFile().mkdir();
                }

                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(mypath);
                    // Use the compress method on the BitMap object to write image to the OutputStream
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    //photoVisitor = mypath.getPath();
                    Log.e("not Inserted0", mypath.getPath());
                } catch (Exception e) {
                    Log.e("myPathError", e.toString());
                    e.printStackTrace();
                } finally {
                    //fos.close();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.e("bitmap failed", "bitmap failed");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.e("ed", "bitmap failed");
            }
        });
    }

}
