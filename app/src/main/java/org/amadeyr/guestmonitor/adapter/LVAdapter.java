package org.amadeyr.guestmonitor.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.amadeyr.guestmonitor.R;
import org.amadeyr.guestmonitor.VisitorDetails;
import org.amadeyr.guestmonitor.database.DBForOff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tushar on 7/28/2016.
 */
public class LVAdapter extends BaseAdapter {

    ArrayList name, dateTime, photo, exitTime, id, whereToGo, phone;
    Context context;
    LayoutInflater inflater;
    private static Animation animation;
    private int lastPosition;
    DBForOff dbo;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public LVAdapter(Context context, ArrayList name, ArrayList dateTime, ArrayList exitTime, ArrayList photo, ArrayList id, ArrayList whereToGo, ArrayList phone){
        this.context = context;
        this.name = name;
        this.dateTime = dateTime;
        this.photo = photo;
        this.id = id;
        this.exitTime = exitTime;
        this.phone = phone;
        this.whereToGo = whereToGo;
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dbo = new DBForOff(context);
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //final Holder holder;
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row, null);
            holder = new Holder();
            holder.pro = (CircleImageView) convertView.findViewById(R.id.profile_image);
            holder.name = (TextView) convertView.findViewById(R.id.textView);
            holder.dateTime = (TextView) convertView.findViewById(R.id.textView2);
            holder.b = (Button) convertView.findViewById(R.id.button3);
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        holder.name.setText("" + name.get(position));
        StringBuilder str = new StringBuilder("" + dateTime.get(position));
        holder.dateTime.setText(str.insert(12, "\n").toString());
        //Picasso.with(context).load("" + photo.get(position)).fit().centerCrop().into(holder.pro);
        if((("" + photo.get(position)).startsWith("http"))){
            Picasso.with(context).load("" + photo.get(position)).fit().centerCrop().into(holder.pro);
        }else{
            Picasso.with(context).load(new File("" + photo.get(position))).fit().centerCrop().into(holder.pro);
        }

        if(exitTime.get(position).equals("inside")){
            holder.b.setText("Inside");
            holder.b.setBackgroundResource(R.drawable.mybuttonaccent);
        }else {
            holder.b.setText("" + whereToGo.get(position));
            holder.b.setBackgroundResource(R.drawable.mybutton);

        }


        holder.b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(("" + exitTime.get(position)).equals("inside")){
                    requestUpdate("" + id.get(position), position);
                    holder.b.setBackgroundResource(R.drawable.mybutton);
                    holder.b.setText("" + whereToGo.get(position));
                }else {
                    Toast.makeText(context, "This person has left the house", Toast.LENGTH_SHORT).show();

                }
            }
        });

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, VisitorDetails.class).putExtra("phone", "" + phone.get(position)).putExtra("photo", "" + photo.get(position)));
            }
        });

        return convertView;
    }

    private void requestUpdate(final String id, final int pos) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/updateExit.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", response);
                        if(response.equals("1")){
                            //holder.b.setText("" + whereToGo.get(pos));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", sp.getString("userName", ""));
                params.put("exitTime", getDate());
                params.put("id", id);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public String getDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm a");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    class Holder{
        CircleImageView pro;
        TextView name, dateTime;
        Button b;
    }

}
