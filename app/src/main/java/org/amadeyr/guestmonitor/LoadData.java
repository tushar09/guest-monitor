package org.amadeyr.guestmonitor;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.amadeyr.guestmonitor.adapter.LVAdapter;
import org.amadeyr.guestmonitor.database.DBForOff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class LoadData extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    DBForOff dbo;

    ListView lv;
    ArrayList name, dateTime, exitTime, phone, photo, whereToGo;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList id;
    private MenuItem actionbarMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbo = new DBForOff(this);

        sp = getSharedPreferences("utils", MODE_PRIVATE);
        editor = sp.edit();

        lv = (ListView) findViewById(R.id.lv);
        name = new ArrayList();
        photo = new ArrayList();
        dateTime = new ArrayList();
        exitTime = new ArrayList();
        phone = new ArrayList();
        id = new ArrayList();
        whereToGo = new ArrayList();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent );


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoadData.this, GuestProfile.class));
            }
        });
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        swipeRefreshLayout.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        loadData();
                                    }
                                }, 1000
        );
    }

    private void loadData() {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/loadData.php",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        //Log.e("duplicated detected", response);
//                        JSONObject jsonObject1 = null;
//
//                        name = new ArrayList();
//                        photo = new ArrayList();
//                        dateTime = new ArrayList();
//                        exitTime = new ArrayList();
//                        phone = new ArrayList();
//                        id = new ArrayList();
//                        whereToGo = new ArrayList();
//
//                        try {
//                            jsonObject1 = new JSONObject(response);
//                            JSONArray jArr = jsonObject1.getJSONArray("FullData");
//                            //JSONObject data = jArr.getJSONObject(0);
//
//                            for (int t = 0; t < jArr.length(); t++) {
//                                JSONObject jsonObject = jArr.getJSONObject(t);
//                                name.add(jsonObject.get("name"));
//                                dateTime.add(jsonObject.get("dateTime"));
//                                phone.add(jsonObject.get("phone"));
//                                //photo.add(jsonObject.get("photo"));
//                                photo.add(jsonObject.get("photo"));
//                                exitTime.add(jsonObject.get("exitTime"));
//                                id.add(jsonObject.get("id"));
//                                whereToGo.add(jsonObject.get("whereToGo"));
//
//
//                            }
//
//                            lv.setAdapter(new LVAdapter(LoadData.this, name, dateTime, exitTime, photo, id, whereToGo, phone));
//                            swipeRefreshLayout.setRefreshing(false);
//
//                            //if(response.equals("yes")){
//                            //}else {
////                            AlertDialog.Builder builder = new AlertDialog.Builder(LoadData.this);
////                            builder.setTitle("Funny Videos")
////                                    .setMessage("Sorry, server under maintenance. Please wait for 1 hour.")
////                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
////                                        public void onClick(DialogInterface dialog, int which) {
////                                            //finish();
////                                        }
////                                    })
////                                    .setIcon(R.mipmap.ic_launcher);
////                            final AlertDialog a = builder.create();
////                            a.setOnShowListener(new DialogInterface.OnShowListener() {
////                                @Override
////                                public void onShow(DialogInterface dialog) {
////                                    a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
////                                }
////                            });
////                            a.show();
//                            //}
//                        } catch (JSONException e) {
//                            Log.e("error", e.toString());
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loadOffLine();
//                        Toast.makeText(LoadData.this, "Loaded off line data", Toast.LENGTH_SHORT).show();
////                        AlertDialog.Builder builder = new AlertDialog.Builder(LoadData.this);
////                        builder.setTitle("Guest Monitor")
////                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
////                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int which) {
////                                        finish();
////                                    }
////                                })
////                                .setIcon(R.mipmap.ic_launcher);
////                        final AlertDialog a = builder.create();
////                        a.setOnShowListener(new DialogInterface.OnShowListener() {
////                            @Override
////                            public void onShow(DialogInterface dialog) {
////                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
////                            }
////                        });
////                        a.show();
//                        //dfinish();
//                    }
//                }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("username", sp.getString("userName", ""));
//                Log.e("username", sp.getString("userName", ""));
//                return params;
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
        loadOffLine();
    }

    private void loadOffLine() {
        name = new ArrayList();
        whereToGo = new ArrayList();
        id = new ArrayList();
        exitTime = new ArrayList();
        photo = new ArrayList();
        phone = new ArrayList();
        dateTime = new ArrayList();
        Cursor cursor = dbo.getAllData();
        if (cursor.moveToFirst()){
            do{
                name.add(cursor.getString(cursor.getColumnIndex("name")));
                Log.e("name", cursor.getString(cursor.getColumnIndex("name")));
                dateTime.add(cursor.getString(cursor.getColumnIndex("dateTime")));
                Log.e("dateTime", cursor.getString(cursor.getColumnIndex("dateTime")));
                phone.add(cursor.getString(cursor.getColumnIndex("phone")));
                Log.e("phone", cursor.getString(cursor.getColumnIndex("phone")));
                photo.add(cursor.getString(cursor.getColumnIndex("photo")));
                Log.e("photo", cursor.getString(cursor.getColumnIndex("photo")));
                exitTime.add(cursor.getString(cursor.getColumnIndex("exitTime")));
                Log.e("exitTime", cursor.getString(cursor.getColumnIndex("exitTime")));
                id.add(cursor.getInt(cursor.getColumnIndex("id")));
                Log.e("id", cursor.getString(cursor.getColumnIndex("id")));
                whereToGo.add(cursor.getString(cursor.getColumnIndex("wantToGo")));
                Log.e("wantToGo", cursor.getString(cursor.getColumnIndex("wantToGo")));
                // do what ever you want here
            }while(cursor.moveToNext());
        }
        cursor.close();
        lv.setAdapter(new LVAdapter(LoadData.this, name, dateTime, exitTime, photo, id, whereToGo, phone));
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //final int searchBarId = searchView.getContext().getResources().getIdentifier("android:id/search_bar", null, null);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 2000);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            Log.e("search erroe", e.toString());
        }

        MenuItem mi = menu.findItem(R.id.search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {

                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {

                        return true; // Return true to expand action view
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Search(newText);
                return true;
            }
        });


        return true;

    }

    private void Search(String newText) {
        ArrayList name, dateTime, exitTime, phone, photo, whereToGo;
        name = new ArrayList();
        dateTime = new ArrayList();
        exitTime = new ArrayList();
        phone = new ArrayList();
        photo = new ArrayList();
        whereToGo = new ArrayList();

        if (this.name != null && this.name.size() != 0) {
            for (int t = 0; t < this.name.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.name.get(t)).find()) {
                    name.add(this.name.get(t));
                    dateTime.add(this.dateTime.get(t));
                    exitTime.add(this.exitTime.get(t));
                    phone.add(this.phone.get(t));
                    photo.add(this.photo.get(t));
                    whereToGo.add(this.whereToGo.get(t));
                }
            }
        }
        lv.setAdapter(new LVAdapter(LoadData.this, name, dateTime, exitTime, photo, id, whereToGo, phone));
        //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
        //lv.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.sync) {
            Sync sync = new Sync(LoadData.this);
            sync.fillOffDB();
            return true;
        }
        if (id == R.id.delete) {
            startDeleteOperation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startDeleteOperation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoadData.this);
        builder.setTitle("Guest Monitor")
                .setMessage("Do You Want To Delete All The Data?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dbo.deleteAllData();
                        Toast.makeText(LoadData.this, "All Data Is Deleted Successfully", Toast.LENGTH_SHORT).show();

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/killSync.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                }) {

                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("username", sp.getString("userName", ""));
                                Log.e("username", sp.getString("userName", ""));
                                return params;
                            }

                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(LoadData.this);
                        requestQueue.add(stringRequest);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                })
                .setIcon(R.mipmap.ic_launcher);
        final AlertDialog a = builder.create();
        a.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        a.show();
    }


}
