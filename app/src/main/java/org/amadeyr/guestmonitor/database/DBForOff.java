package org.amadeyr.guestmonitor.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.amadeyr.guestmonitor.adapter.LVAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tushar on 8/4/2016.
 */
public class DBForOff extends SQLiteOpenHelper {

    public final static String DB_NAME = "homemonitor.db";
    public final String TABLE_NAME = "visitor";

    public final String ID = "id";
    public final String NAME = "name";
    public final String PHONE = "phone";
    public final String CAMEFROM = "cameFrom";
    public final String WANTTOGO = "wantToGo";
    public final String PEOPLETOMEET = "peopleToMeeet";
    public final String DATETIME = "dateTime";
    public final String EXITTIME = "exitTime";
    public final String USERID = "userId";
    public final String PHOTO = "photo";
    public final String SYNCFLAG = "syncFlag";

    private Context context;


    public DBForOff(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, " + NAME + " TEXT, " + PHONE + " TEXT, " + CAMEFROM + " TEXT, " + WANTTOGO + " TEXT, " + PEOPLETOMEET + " TEXT, " + DATETIME + " TEXT, " + EXITTIME + " TEXT, " + USERID + " TEXT, " + PHOTO + " TEXT, " + SYNCFLAG + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean insertRow(String NAME, String PHONE, String CAMEFROM, String WANTTOGO, String PEOPLETOMEET, String DATETIME, String EXITTIME, String USERID, String PHOTO, String SYNCFLAG) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.NAME, NAME);
        contentValues.put(this.PHONE, PHONE);
        contentValues.put(this.CAMEFROM, CAMEFROM);
        contentValues.put(this.WANTTOGO, WANTTOGO);
        contentValues.put(this.PEOPLETOMEET, PEOPLETOMEET);
        contentValues.put(this.DATETIME, DATETIME);
        contentValues.put(this.EXITTIME, EXITTIME);
        contentValues.put(this.USERID, USERID);
        contentValues.put(this.PHOTO, PHOTO);
        contentValues.put(this.SYNCFLAG, SYNCFLAG);

        if(checkIsDataAlreadyInDBorNot(DATETIME, USERID, PHONE)){
            long r = db.insert(TABLE_NAME, null, contentValues);
            if (r == -1) {
                //Log.e("not inserted", "not inserted");
                return false;
            } else {
                //Log.e("inserted", "inserted");
                return true;
            }
        }else{
            //Log.e("duplicated detected", "duplicated detected");
            return false;
        }

    }

    public boolean checkIsDataAlreadyInDBorNot(String dateTime, String userID, String phone) {
        dateTime = "'" + dateTime + "'";
        userID = "'" + userID + "'";
        phone = "'" + phone + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_NAME + " where " + DATETIME + " = " + dateTime + " and " + USERID + " = " + userID + " and " + PHONE + " = " + phone;
        //Log.e("inserted", Query);
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    public void deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
    }

    public void deleteDuplicate(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE l1 FROM visitor l1, visitor l2 WHERE l1.id > l2.id AND l1.dateTime = l2.dateTime AND l1.userId = l2.userId AND l1.phone = l2.phone");
        //db.rawQuery("DELETE FROM visitor USING visitor, visitor as vvisitor WHERE (visitor.id > vvisitor.id) AND (visitor.dateTime = vvisitor.dateTime) AND (visitor.userId = vvisitor.userId) AND (visitor.phone = vvisitor.phone)", null);
        //delete from visitor where ()
        //db.rawQuery("Select min(n1.id) from visitor n1 join visitor n2 on n1.dateTime = n2.dateTime and n1.userId = n2.userId AND n1.phone = n2.phone", null);
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from visitor order by " + DATETIME + " DESC", null);
        return c;
    }

    public Cursor getVisitorDetails(String phone){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from visitor where phone" + " = " + phone + " order by " + DATETIME + " DESC", null);
        return c;
    }

    public void syncRequest() throws JSONException {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from visitor where syncFlag = '0'", null);
        JSONObject innerObject = new JSONObject();
        JSONObject outerObject = new JSONObject();
        JSONArray outerArray = new JSONArray();
        ArrayList<String> id = new ArrayList<String>();
        if (c.moveToFirst()) {

            do {
                id.add(c.getString(c.getColumnIndex("id")));
                //String id = c.getString(c.getColumnIndex("id"));
                innerObject.put("name", c.getString(c.getColumnIndex("name")));
                innerObject.put("phone", c.getString(c.getColumnIndex("phone")));
                innerObject.put("cameFrom", c.getString(c.getColumnIndex("cameFrom")));
                innerObject.put("wantToGo", c.getString(c.getColumnIndex("wantToGo")));
                innerObject.put("peopleToMeeet", c.getString(c.getColumnIndex("peopleToMeeet")));
                innerObject.put("dateTime", c.getString(c.getColumnIndex("dateTime")));
                innerObject.put("exitTime", c.getString(c.getColumnIndex("exitTime")));
                innerObject.put("userId", c.getString(c.getColumnIndex("userId")));
                //db.rawQuery("UPDATE visitor SET syncFlag = '1' where id = " + id, null);
                innerObject.put("syncFlag", "1");

                ContextWrapper cw = new ContextWrapper(context);
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmapFront = BitmapFactory.decodeFile(getFilePath(c.getString(c.getColumnIndex("phone"))), options);

                if(bitmapFront == null){
                    Log.e("bitmap", "null");
                    innerObject.put("photo", c.getString(c.getColumnIndex("photo")));
                }else {
                    innerObject.put("photo", encodeToBase64(bitmapFront, Bitmap.CompressFormat.JPEG, 100));
                    Log.e("bitmap", "not null" );
                }

            } while (c.moveToNext());
        }else{
            Log.e("visitor", "no first");
        }

        outerArray.put(innerObject);
        outerObject.put("visitor", outerArray);
        Log.e("asdf", outerObject.toString());
        String res = sendData(outerObject.toString());
        if(res.equals("Done")){
            for(int t = 0; t < id.size(); t++){
                db.rawQuery("UPDATE visitor SET syncFlag = '1' where id = " + id.get(t), null);
            }
        }
    }

    public String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);

        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    String getFilePath(String phone){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File file[] = directory.listFiles();
        Log.e("Files", "Size: "+ file.length);
        String photoFront = "";
        String photoBack = "";

        boolean f = false;
        boolean b = false;

        for (int i=0; i < file.length; i++) {
//                if(file[i].getName().startsWith(phn.getText().toString()) && file[i].getName().endsWith("front.jpg")){
//                    photoFront = file[i].getPath();
//                    f = true;
//                }
//                if(file[i].getName().startsWith(phn.getText().toString()) && file[i].getName().endsWith("back.jpg")){
//                    photoBack = file[i].getPath();
//                    b = true;
//                }
            if(file[i].getName().equals(phone + ".jpg")){
                String photoVisitor = file[i].getPath();
                return photoVisitor;
                //b = true;
            }
        }

        return null;

    }

    String res = "";
    protected final String sendData(final String json) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/homemonitor/sync.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        res = response;
                        Log.e("die", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("json", json);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
        return res;
    }

}
